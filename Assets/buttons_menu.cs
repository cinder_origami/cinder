﻿using UnityEngine;
using System.Collections;

public class buttons_menu : buttons 
{
    public MAIN_MENU main_buttons;
    public enum MAIN_MENU
    {
        INVALID =-1,
        START_GAME,
        QUIT
    }
    BoxCollider2D m_box_collider_2d;
    ////////////////////////////////////////////////////////////////////////////////////////////////

    void Start () 
	{
        m_box_collider_2d = GetComponent<BoxCollider2D>();

    }

	////////////////////////////////////////////////////////////////////////////////////////////////

	void Update () 
	{
	
	}

    ////////////////////////////////////////////////////////////////////////////////////////////////
    public override void handle_press()
    {
        switch (main_buttons)
        {
            case MAIN_MENU.START_GAME:
                Application.LoadLevel(1);
                break;
            case MAIN_MENU.QUIT:
                Application.Quit();
                break;
        }
    }
    public override BoxCollider2D get_box_collider() { return m_box_collider_2d; }
}
