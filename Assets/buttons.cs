﻿using UnityEngine;
using System.Collections;

public abstract class buttons : MonoBehaviour
{
    abstract public void handle_press();
    abstract public BoxCollider2D get_box_collider();
}
