﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class buttons_manager : MonoBehaviour
{
    bool m_can_test_for_button_clicks;
    public bool can_test_for_button_clicks
    {
        get { return m_can_test_for_button_clicks;  }
        set { m_can_test_for_button_clicks = value; }
    }

    public List<buttons> m_buttons = new List<buttons>();

    ///////////////////////////////////////////////////////////////////////////////

    void Start ()
    {
        m_can_test_for_button_clicks = true;
    }

    ///////////////////////////////////////////////////////////////////////////////

    void Update ()
    {
	    if(m_can_test_for_button_clicks && Input.GetMouseButtonDown(0))
        {
            //Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            Vector2 mouse_click_pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);

            for (int i=0; i < m_buttons.Count; i++)
            {
                //Vector3 mounse_click = ray.GetPoint(m_buttons[i].transform.position.z);                

                if (m_buttons[i].get_box_collider().bounds.Contains(mouse_click_pos))
                {
                    m_buttons[i].handle_press();
                    return;
                }
            }
        }
	}

    ///////////////////////////////////////////////////////////////////////////////

}
