﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class platform_simple : MonoBehaviour
{
    public float m_move_duration;

    public bool m_wait_for_player;
    public bool m_reset_after_use;
    public bool m_path_only_once;
    public bool m_dont_parent;
    public bool m_reset_to_orig_parent;

    public List<Vector2> m_movement_path_nodes;
    public Transform m_the_parent;

    Transform m_orig_parent;

    Vector2 m_starting_pos;
    bool m_shoud_move;
    bool m_has_been_used;
    bool m_is_in_reverse;
    int m_path_node_index;
    float m_time;
    float m_phase;

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////

    void Start ()
    {
        m_path_node_index = 0;
        m_is_in_reverse = false;
		m_starting_pos = transform.localPosition;

        if (m_wait_for_player == false)
        {
            m_shoud_move = true;
            m_has_been_used = true;
        }            
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////

    void Update ()
    {
        if (m_shoud_move)
        {
            if (Mathf.Abs(transform.localPosition.magnitude - m_movement_path_nodes[m_path_node_index].magnitude) < 0.002f)
            {
                if (m_is_in_reverse == false)
                {
                    m_path_node_index++;
					m_starting_pos = transform.localPosition;
                   
                    if (m_path_node_index > (m_movement_path_nodes.Count - 1))
                    {
                        m_is_in_reverse = true;
                        m_path_node_index = m_movement_path_nodes.Count - 2;

                        if (m_path_only_once == true)
                        {
                            m_path_only_once = false;
                            m_shoud_move = false;
                        }
                    }
                }
                else
                {
                    m_path_node_index--;
					m_starting_pos = transform.localPosition;

                    if (m_path_node_index < 0)
                    {
                        m_is_in_reverse = false;
                        m_path_node_index = 1;
                    }
                }

                m_time = 0;
            }
            else
            {
                m_time += Time.deltaTime * ( 1 / m_move_duration);

                transform.localPosition = 
					Vector3.Lerp(m_starting_pos, m_movement_path_nodes[m_path_node_index], m_time);
            }
        }       
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.transform.tag.Equals("Player"))
        {
            if (m_wait_for_player == true && m_has_been_used == false && m_path_only_once == true)
            {                
                m_shoud_move = true;
                m_has_been_used = true;
            }
            else if (m_wait_for_player == true && m_wait_for_player == false)
            {
                m_shoud_move = true;
                m_has_been_used = true;
            }
            else if (m_wait_for_player == true)
            {
                m_shoud_move = true;
                m_has_been_used = true;
            }

            m_orig_parent = collision.transform.parent;

            if (m_the_parent != null && m_dont_parent == false)
                collision.transform.parent = m_the_parent;
            else if (m_dont_parent == false)
                collision.transform.parent = transform;
        }
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.transform.tag.Equals("Player") && m_dont_parent == false)
        {
            if(m_reset_to_orig_parent)
                collision.transform.parent = m_orig_parent;
            else
                collision.transform.parent = null;
        }
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////

}

