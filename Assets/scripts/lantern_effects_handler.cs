﻿using UnityEngine;
using System.Collections;

public class lantern_effects_handler : MonoBehaviour
{
    public Transform m_glow_inner;
    public Vector2 m_glow_inner_min_scale;
    public Vector2 m_glow_inner_max_scale;   
	public float m_glow_inner_scale_duration;  
   
	Vector2 m_glow_inner_starting_scale;   
	bool m_inner_glow_up;
    float m_time;
    float m_time_glow_inner;

    ////////////////////////////////////////////////////////////////////////////////////////////////

    void Start ()
    {
		m_glow_inner_starting_scale = m_glow_inner.localScale;
	}

    ////////////////////////////////////////////////////////////////////////////////////////////////

    void Update ()
    {
		m_time += Time.deltaTime;

        update_inner_glow();
        update_outter_glow();

        /*
        m_phase = Mathf.Sin(m_time / 60);

        transform.position =
            Vector3.Lerp(transform.position, m_movement_path_nodes[m_path_node_index], m_phase);
        */
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////

    void update_inner_glow()
    {
		m_time_glow_inner += Time.deltaTime * (1/m_glow_inner_scale_duration);

        if (m_inner_glow_up)
        {
            m_glow_inner.localScale =
				Vector2.Lerp(m_glow_inner_starting_scale, m_glow_inner_max_scale, m_time_glow_inner);

            if (Mathf.Abs(m_glow_inner.localScale.magnitude - m_glow_inner_max_scale.magnitude) < 0.02f)
            {
                m_inner_glow_up = !m_inner_glow_up;
                m_time_glow_inner = 0;
				m_glow_inner_starting_scale = m_glow_inner.localScale;
            }
                
        }        
        else if(!m_inner_glow_up)
        {
           m_glow_inner.localScale =
				Vector2.Lerp(m_glow_inner_starting_scale, m_glow_inner_min_scale, m_time_glow_inner);

            if (Mathf.Abs(m_glow_inner.localScale.magnitude - m_glow_inner_min_scale.magnitude) < 0.02f)
            {
                m_inner_glow_up = !m_inner_glow_up;
                m_time_glow_inner = 0;
				m_glow_inner_starting_scale = m_glow_inner.localScale;
            }                
        }        
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////

    void update_outter_glow()
    {
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
}
