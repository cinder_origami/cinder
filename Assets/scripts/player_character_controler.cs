﻿using UnityEngine;
using System.Collections;

public class player_character_controler : MonoBehaviour
{
	Rigidbody2D m_rigidbody;
    BoxCollider2D m_box_2d_collider;
    Animator m_animator;
    public Transform m_modle_transform;

    public float m_jump_power = 250;

    public float m_gravity_multiplier = 1.5f;
    float m_move_speed_multiplier = 0.1f;

    float m_ground_check_distance = 0.2f;
    float m_orig_ground_check_distance;

    bool m_is_grounded;
    bool m_has_storm_started;
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////

    void Start()
    {
        m_is_grounded = true;

        m_orig_ground_check_distance = m_ground_check_distance;
         
        m_rigidbody = GetComponent<Rigidbody2D>();
        m_box_2d_collider = GetComponent<BoxCollider2D>();
        m_animator = GetComponent<Animator>();        
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public void Move(Vector3 move)
    {
        if (move.magnitude > 1f)
            move.Normalize();

        CheckGroundStatus();

        if(m_is_grounded == false)      
            HandleAirborneMovement(move);     

        else if (m_is_grounded)
        {
            Vector3 v = (move *  m_move_speed_multiplier / Time.deltaTime);

            v.y = m_rigidbody.velocity.y;
            m_rigidbody.velocity = v;           
        }        
        m_animator.SetFloat("speed_x", move.x);        
        m_animator.SetFloat("speed_y", m_rigidbody.velocity.y);

        if(m_has_storm_started)
        {
            m_rigidbody.AddForce(new Vector2(-100, 0));
        }
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////

    void CheckGroundStatus()
    {
		Vector2 ray_1_start_position;
		Vector2 ray_2_start_position;
		RaycastHit hitInfo_c;

		ray_1_start_position = transform.position - 
			new Vector3(m_box_2d_collider.size.x, m_box_2d_collider.size.y, 0);
		ray_2_start_position = transform.position - 
			new Vector3(-m_box_2d_collider.size.x, m_box_2d_collider.size.y, 0);

		Debug.DrawRay(ray_1_start_position, Vector3.down, Color.yellow);
		Debug.DrawRay(ray_2_start_position, Vector3.down, Color.yellow);

		if (Physics.Raycast(ray_1_start_position, Vector3.down, out hitInfo_c, m_ground_check_distance))
        {
			Debug.DrawRay(ray_1_start_position, Vector3.down, Color.green);
			m_is_grounded = true;      
        }            
		else if (Physics.Raycast(ray_2_start_position, Vector3.down, out hitInfo_c, m_ground_check_distance))
		{
			Debug.DrawRay(ray_2_start_position, Vector3.down, Color.green);
			m_is_grounded = true;      
		}
        else
        {
            m_is_grounded = false;
        }
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////
  
    public void handle_jump(bool jump)
    {
       if(jump)//&& m_is_grounded)
        {  
            m_rigidbody.AddForce(new Vector2(0, m_jump_power));        
            m_is_grounded = false;
            m_ground_check_distance = 0.1f;            
        }
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////

    void HandleAirborneMovement(Vector3 move)
    {
        Vector3 v = (move * m_move_speed_multiplier / Time.deltaTime);
        v.y = m_rigidbody.velocity.y;
        
		m_rigidbody.velocity = v;      

        Vector3 extraGravityForce = (Physics.gravity * m_gravity_multiplier) - Physics.gravity;        

		//m_rigidbody.AddForce(extraGravityForce);
        m_ground_check_distance = m_rigidbody.velocity.y < 0 ? m_orig_ground_check_distance : 0.01f;        
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public void storm_resistance_start()
    {        
		Invoke ("inv_storm_resistance_start", 1.5f);        
    }

	void inv_storm_resistance_start()
	{
		m_has_storm_started = true;
	}

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public void storm_resistance_stop()
    {
        m_has_storm_started = false;
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////
}
