﻿using UnityEngine;
using System.Collections;

public class player_user_controler : MonoBehaviour
{
    player_character_controler m_character;
    BoxCollider2D m_character_box_collide_2d;

    Vector3 m_move;                             
    Vector3 m_forward;

    bool m_can_jump;
    bool m_stealth_enabled;

    float m_input_h;
    float m_input_v;

    
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////

    void Start()
    {
        m_character = GetComponent<player_character_controler>();
        m_character_box_collide_2d = GetComponent<BoxCollider2D>();
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
            m_character.handle_jump(true);
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////

    private void FixedUpdate()
    {       
        //////////////////
        // reset variables
        //////////////////
        m_input_h = 0;
        m_input_v = 0;
        m_move = Vector3.zero;

        ////////////////////////
        // check for key presses
        ////////////////////////        
        if (Input.GetKey(KeyCode.W))
            m_input_v += 1;
        if (Input.GetKey(KeyCode.S))
            m_input_v += -1;
        if (Input.GetKey(KeyCode.A))
            m_input_h = -1;
        if (Input.GetKey(KeyCode.D))
            m_input_h = 1;

        if (Input.GetKey(KeyCode.E))
            interact();

        //////////
        // Grab 
        //////////
        m_forward = Vector3.Scale(transform.forward, new Vector3(1, 0, 1)).normalized;
        m_move = m_input_v * m_forward + m_input_h * transform.right;

        m_character.Move(m_move);
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////

    void interact()
    {
        Vector2 ray_1_start_position;
        RaycastHit2D hitInfo;

        ray_1_start_position = transform.position + new Vector3(((m_character_box_collide_2d.size.x / 2) + 0.1f), 0, 0);
        
        Debug.DrawRay(ray_1_start_position, Vector3.right, Color.yellow);
        hitInfo = Physics2D.Raycast(ray_1_start_position, Vector3.right, 2);

        if (hitInfo)
        {            
            npc temp_npc = hitInfo.transform.GetComponent<npc>();            

            if (temp_npc != null)            
                temp_npc.start_conversation();            
        }
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////

}
