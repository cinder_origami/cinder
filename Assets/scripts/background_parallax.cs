﻿using UnityEngine;
using System.Collections;

public class background_parallax : MonoBehaviour
{
    public Vector2 m_speed_sacle;

    Renderer m_background_renderer;
    Vector2 m_starting_position;    

    ////////////////////////////////////////////////////////////////////////////////////////////////

    void Start ()
    {
        m_starting_position = transform.position;
        m_background_renderer = GetComponent<Renderer>();
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////

    void Update ()
    {
        Vector2 diff = (Vector2)transform.position - m_starting_position;
        diff.Scale(m_speed_sacle);

        m_background_renderer.material.mainTextureOffset = diff;   
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
}
