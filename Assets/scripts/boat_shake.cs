﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class boat_shake : MonoBehaviour
{
    public List<Vector3> m_rotation_angles;
    public float m_rotation_duration;

    //public bool m_dont_parent;
    //public bool m_reset_to_orig_parent;

    //Transform m_orig_parent;

    bool m_is_in_reverse;
    int m_path_node_index;
    Vector3 m_starting_rotation;
    float m_time;

    ////////////////////////////////////////////////////////////////////////////////////////////////

    void Start ()
    {
	
	}

    ////////////////////////////////////////////////////////////////////////////////////////////////

    void Update ()
    {
        if (Mathf.Abs(transform.eulerAngles.magnitude - m_rotation_angles[m_path_node_index].magnitude) < 0.002f)
        {
            if (m_is_in_reverse == false)
            {
                m_path_node_index++;
                m_starting_rotation = transform.eulerAngles;

                if (m_path_node_index > (m_rotation_angles.Count - 1))
                {
                    m_is_in_reverse = true;
                    m_path_node_index = m_rotation_angles.Count - 2;

                    /*
                    if (m_path_only_once == true)
                    {
                        m_path_only_once = false
                        m_shoud_move = false;
                    }
                    */
                }
            }
            else
            {
                m_path_node_index--;
                m_starting_rotation = transform.eulerAngles;

                if (m_path_node_index < 0)
                {
                    m_is_in_reverse = false;
                    m_path_node_index = 1;
                }
            }

            m_time = 0;
        }
        else
        {
            m_time += Time.deltaTime * (1 / m_rotation_duration);

            transform.eulerAngles =
                Vector3.Lerp(m_starting_rotation, m_rotation_angles[m_path_node_index], m_time);
        }
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.transform.tag.Equals("Player"))
        {
            /*
            if (m_wait_for_player == true && m_has_been_used == false && m_path_only_once == true)
            {
                m_shoud_move = true;
                m_has_been_used = true;
            }
            else if (m_wait_for_player == true && m_wait_for_player == false)
            {
                m_shoud_move = true;
                m_has_been_used = true;
            }
            else if (m_wait_for_player == true)
            {
                m_shoud_move = true;
                m_has_been_used = true;
            }
            */

            /*
            m_orig_parent = collision.transform.parent;
            if (m_the_parent != null && m_dont_parent == false)
                collision.transform.parent = m_the_parent;
            else if (m_dont_parent == false)
                collision.transform.parent = transform;
            */
        }
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////

    void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.transform.tag.Equals("Player") )//&& m_dont_parent == false)
        {
            collision.transform.eulerAngles = Vector3.zero;
            /*
            if (m_reset_to_orig_parent)
                collision.transform.parent = m_orig_parent;
            else
                collision.transform.parent = null;
             */
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
}
