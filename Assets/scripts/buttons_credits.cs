﻿using UnityEngine;
using System.Collections;

public class buttons_credits : buttons
{
    public BUTTON_TYPE_CREDITS m_button_type;
    public enum BUTTON_TYPE_CREDITS
    {
        INVALID =-1,
        GO_TO_MAIN,
        EXIT
    }

    BoxCollider2D m_box_collider_2d;

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////

    void Start ()
    {
        m_box_collider_2d = GetComponent<BoxCollider2D>();
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////

    void Update ()
    {
	
	}

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////

    override public void handle_press()
    {
        switch(m_button_type)
        {
            case BUTTON_TYPE_CREDITS.GO_TO_MAIN:
                Application.LoadLevel(0);
                break;
            case BUTTON_TYPE_CREDITS.EXIT:
                Application.Quit();
                break;
        }
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////

    override public BoxCollider2D get_box_collider()
    {
        return m_box_collider_2d;
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////
}
